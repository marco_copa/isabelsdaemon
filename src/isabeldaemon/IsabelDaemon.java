/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package isabeldaemon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Marco
 */
public class IsabelDaemon {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
                String s = null;

                try {

                        // Determinar en qué SO estamos
                        String so = System.getProperty("os.name");

                        String[] comando  = new String [3];

                        // Comando para Linux
                        if (so.equals("Linux"))
                        {       comando[0] = "ifconfig";
                                comando[1] = "ifconfig";
                                comando[2] = "ifconfig";
                        // Comando para Windows
                        }else{
                            
                            comando[0] = "cmd";
                            comando[1] = "ifconfig";
                            comando[2] = "ifconfig";
                        }
                                
                        // Ejcutamos el comando
                        //Process p1 = Runtime.getRuntime().exec("cmd cd /c /c/Users/Marco/git/qa/autotests/tests/ota");
                        //Process p0 = Runtime.getRuntime().exec("cmd");
                       Process p2 = Runtime.getRuntime().exec("ruby.exe /c C:\\Users\\Marco\\git\\qa\\autotests\\tests\\ota\\edc_login_flow_149");
                        //Process p2 = Runtime.getRuntime().exec("cmd  /c cd C:\\Users\\Marco\\git\\qa\\autotests\\tests\\ota &&ruby edc_login_flow_1494.rb");
                         //Process p4 = Runtime.getRuntime().exec("cmd /c cd");
                        BufferedReader stdInput = new BufferedReader(new InputStreamReader(
                                        p2.getInputStream()));
                        BufferedReader stdError = new BufferedReader(new InputStreamReader(
                                        p2.getErrorStream()));

                        // Leemos la salida del comando
                        System.out.println("Ésta es la salida standard del comando:\n");
                        while ((s = stdInput.readLine()) != null) {
                                System.out.println(s);
                        }
                        // Leemos los errores si los hubiera
                        System.out
                                        .println("Ésta es la salida standard de error del comando (si la hay):\n");
                        while ((s = stdError.readLine()) != null) {
                                System.out.println(s);
                        }
                        System.exit(0);
                } catch (IOException e) {
                        System.out.println("Excepción: ");
                        e.printStackTrace();
                        System.exit(-1);
                }
    }
    
}
